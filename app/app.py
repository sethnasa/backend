from predictions.predictions import Predictions
from flask import Flask, request
from flask_restful import Resource, Api
from pymongo import MongoClient

server = Flask(__name__)
api = Api(server)


@server.route('/')
def home():
    return('Welcome to SETH API!')


api.add_resource(Predictions, '/predictions/', endpoint='post')
api.add_resource(Predictions, '/predictions/<string:name>', endpoint='get')
api.add_resource(Predictions, '/predictions/<string:name>/predict', endpoint='predict')
