import json
import numpy as np
from flask import request
from os import environ as env
from pymongo import MongoClient
from flask_restful import Resource
from common.encoder import JSONEncoder

client = MongoClient(
    'mongodb://{}:{}@{}/?authSource=admin&authMechanism=SCRAM-SHA-1'.format(
        env.get('DB_USER', 'mongo'), env.get('DB_PASS', 'mongo'),
        env.get('DB_HOST', 'localhost'),
    ))
db = client[env.get('DB_NAME', 'db')]

class Predictions(Resource):
    def get(self, name):
        prediction = db.predictions.find_one({'name': name})
        res = json.loads(JSONEncoder().encode(prediction))
        return(res)

    def post(self):
        req = request.get_json(force=True)
        collection = db.predictions
        collection.insert([req])

        return('Prediction added!')
    
    def predict(self, name):
        prediction = db.predictions.find_one({'name': name})
        req = request.get_json(force=True)
        data = json.loads(JSONEncoder().encode(prediction))
        # coeff_matrix = np.array(data['coeff']['betha'])
        # y = data['']
        print(data['coeff']['betha_0'])
        return("Done")

